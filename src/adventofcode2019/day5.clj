(ns adventofcode2019.day5
  (:require [adventofcode2019.core :as core]
            [clojure.string :as str]))

(defn iv
  "Get instruction value"
  [offset {:keys [mode pr ip] :as com}]
  (let [p (get pr (+ offset ip))]

    (cond
      (= 0 (get mode (dec offset)))
      (get pr p)

      (= 1 (get mode (dec offset)))
      p)))

(defn pv
  "Get place to put value"
  [offset {:keys [mode pr ip]}]
  (get pr (+ offset ip)))

(defn add
  [{:keys [pr ip state mode] :as computer}]
  (let [p1 (iv 1 computer)
        p2 (iv 2 computer)
        p3 (pv 3 computer)]
    (assoc-in computer [:pr p3]
              (+ p1 p2))))

(defn mult
  [{:keys [pr ip state] :as computer}]
  (let [p1 (iv 1 computer)
        p2 (iv 2 computer)
        p3 (pv 3 computer)]
    (assoc-in computer [:pr p3]
              (* p1 p2))))

(defn input
  [{:keys [pr ip state in] :as computer}]
  (let [p1 (pv 1 computer)]
    (-> computer
        (assoc-in [:pr p1] (first in))
        (update :in rest))))

(defn output
  [{:keys [pr ip state mode] :as computer}]

  (let [p1 (iv 1 computer)]
    (update computer :out conj p1)))

(defn jump-if-true
  [{:keys [pr ip state mode] :as computer}]

  (let [p1 (iv 1 computer)]
    (if (not (zero? p1))
      (assoc computer :ip (iv 2 computer))
      (update computer :ip #(+ 3 %)))))

(defn jump-if-false
  [{:keys [pr ip state mode] :as computer}]

  (let [p1 (iv 1 computer)]
    (if (zero? p1)
      (assoc computer :ip (iv 2 computer))
      (update computer :ip #(+ 3 %)))))

(defn less-than
  [{:keys [pr ip state mode] :as computer}]

  (let [p1 (iv 1 computer)
        p2 (iv 2 computer)
        p3 (pv 3 computer)]
    (if (< p1 p2)
      (-> computer
          (assoc-in [:pr p3] 1)
          (update :ip #(+ 4 %)))
      (-> computer
          (assoc-in [:pr p3] 0)
          (update :ip #(+ 4 %))))))

(defn equals
  [{:keys [pr ip state mode] :as computer}]

  (let [p1 (iv 1 computer)
        p2 (iv 2 computer)
        p3 (pv 3 computer)]
    (if (= p1 p2)
      (-> computer
          (assoc-in [:pr p3] 1)
          (update :ip #(+ 4 %)))
      (-> computer
          (assoc-in [:pr p3] 0)
          (update :ip #(+ 4 %))))))

(defn opcode
  [i]
  (mod i 100))

(defn modes
  [i]
  (mapv #(Integer. (str %)) (drop 2 (reverse (format "%05d" i)))))

(comment

  (:out (part1))
  )

(defn tick
  [{:keys [pr ip state] :as computer}]

  (let [instruction (opcode (get pr ip))
        computer (assoc computer :mode (modes (get pr ip)) :instruction instruction)]

    (condp = instruction
      1 (update (add computer) :ip #(+ 4 %))
      2 (update (mult computer) :ip #(+ 4 %))
      3 (update (input computer) :ip #(+ 2 %))
      4 (update (output computer) :ip #(+ 2 %))
      5 (jump-if-true computer)
      6 (jump-if-false computer)
      7 (less-than computer)
      8 (equals computer)
      99 (assoc computer :state :done)
      (assoc computer :state :error))))

(defn run
  [computer]
  (if (not= :ok (:state computer))
    computer
    (recur (tick computer))))

(comment
  (run {:ip 0
        :state :ok
        :pr (mapv #(Integer. (str/trim %)) (str/split "1,9,10,3,2,3,11,0,99,30,40,50" #","))})

  )

(defn init-computer
  [memory input]
  (run {:pr memory
        :ip 0
        :state :ok
        :in input
        :out ()}))

(comment
  (init-computer
   [3 0 4 0 99]
   '(10))

  (init-computer
   [1002 4 3 4 33]
   '(10))

  (init-computer
   [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
    1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
    999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]
   '(7))
  )

(defn part1
  []
  (let [computer (init-computer (mapv #(Integer. (str/trim %)) (core/load-csv 5)) '(1))]
    (run computer)))

(defn part2
  []
  (let [computer (init-computer (mapv #(Integer. (str/trim %)) (core/load-csv 5)) '(5))]
    (run computer)))

(comment

  (part1)
  (dissoc (part2) :pr)
  ;;{:ip 676, :state :done, :in (), :out (15724522), :mode [0 0 0], :instruction 99}
  )
