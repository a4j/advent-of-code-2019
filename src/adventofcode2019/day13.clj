(ns adventofcode2019.day13
  (:require [adventofcode2019.core :as core]
            [adventofcode2019.intcode :as intcode]
            [clojure.core.async :as async]
            [clojure.string :as str]))


(defn load
  []
  (map str/trim (core/load-csv 13)))

(defn run-and-get-output
  [program]
  (let [in (async/chan 3)
        out (async/chan 3)
        computer (intcode/init-computer program in out)
        result (async/go-loop [a (async/<! out)
                               result []]
                 (if a
                   (recur (async/<! out) (conj result a))
                   result))]

    (intcode/run computer)
    (async/<!! result)))

(defn parse
  [instructions]
  (partition 3 instructions))

(defn part1
  [program]
  (let [play-area (parse (run-and-get-output program))]
    (count (filter #(= 2 (nth % 2)) play-area))))

(defn extent
  [k]
  {:x-min
   (first (sort (map first k)))
   :x-max
   (first (reverse (sort (map first k))))
   :y-min
   (first (sort (map second k)))
   :y-max
   (first (reverse (sort (map second k))))})

(defn get-tile
  [pa x y]
  (nth (first (filter #(and (= x (first %)) (= y (second %))) pa)) 2))

(defn find-element
  [pa e]
  (first (filter #(= e (nth % 2)) pa)))

(defn tile-char
  [c]
  (condp = c
    0 \space
    1 \x
    2 \█
    3 \_
    4 \b))

(defn play
  [in out]
  (let []

    ))

(defn render
  [pa]
  ;; {:x-min 0N, :x-max 41N, :y-min 0N, :y-max 25N}
  (let [e (extent pa)]
    (doseq [y (range (inc (:y-max e)))]
      (doseq [x (range (inc (:x-max e)))]
        (print (tile-char (int (get-tile pa x y)))))
      (print \newline))))

(defn part2
  [program]
  (let [play-area (parse (run-and-get-output program))]
    (render play-area)))

(comment

  (load)
  (intcode/init-computer (load))
  (part1 (load))
  (part2 (load))

  (let [out (async/chan 3)]
    (async/>!! out 'abc)
    (async/close! out)

    (async/go-loop [a (async/<! out)
                    result []]
      (if a
        (recur (async/<! out) (conj result a))
        result)))

  (async/<!! *1)
  )
