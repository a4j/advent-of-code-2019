(ns adventofcode2019.core
  (:require [clj-http.client :as http]
            [clojure.string :as str]
            [clojure.java.io :as io]))

(defn retrieve
  [day]
  (:body
   (http/get (str "https://adventofcode.com/2019/day/" day "/input")
             {:headers {"Cookie" (str "session=" (slurp "session-id"))}})))

(defn input
  [day]
  (let [fname (str "resources/day" day ".txt")]
    (if (.exists (io/file fname))
      (slurp fname)
      (let [content (retrieve day)]
        (spit fname content)
        content))))

(defn load-lines
  [day]
  (str/split-lines (input day)))

(defn load-csv
  [day]
  (str/split (input day) #","))

(defn fuel
  [n]
  (- (int (/ n 3)) 2))

(defn fuel-with-extra
  [n]
  (loop [prev-val n
         total 0]
    (let [f (fuel prev-val)]
      (if (<= f 0)
        total
        (recur f (+ total f))))))

(defn day1part1
  []

  (let [ls (map #(Integer. %) (load-lines "day1.txt"))]
    (reduce + (map fuel ls))))

(defn day1part2
  []

  (let [ls (map #(Integer. %) (load-lines "day1.txt"))]
    (reduce + (map fuel-with-extra ls))))

(defn p
  [x]
  (prn x)
  x)

(defn add
  [{:keys [pr ip state] :as computer}]
  (let [p1 (get pr (+ 1 ip))
        p2 (get pr (+ 2 ip))
        p3 (get pr (+ 3 ip))]
    (assoc-in computer [:pr p3]
              (+ (get pr p1)
                 (get pr p2)))))

(defn mult
  [{:keys [pr ip state] :as computer}]
  (let [p1 (get pr (+ 1 ip))
        p2 (get pr (+ 2 ip))
        p3 (get pr (+ 3 ip))]
    (assoc-in computer [:pr p3]
              (* (get pr p1)
                 (get pr p2)))))

(comment
  (mult {:pr [1 0 1 3] :ip 0})
  )

(defn tick
  [{:keys [pr ip state] :as computer}]
  (let [instruction (get pr ip)]
    (condp = instruction
      1 (update (add computer) :ip #(+ 4 %))
      2 (update (mult computer) :ip #(+ 4 %))
      99 (assoc computer :state :done)
      (assoc computer :state :error))))

(defn run
  [computer]
  (if (not= :ok (:state computer))
    computer
    (recur (tick computer))))

(comment
  (run {:ip 0
        :state :ok
        :pr (mapv #(Integer. (str/trim %)) (str/split "1,9,10,3,2,3,11,0,99,30,40,50" #","))})

  )

(defn init-computer
  [memory noun verb]
  (let [program memory
        program (assoc program 1 noun)
        program (assoc program 2 verb)]
    (run {:pr program
          :ip 0
          :state :ok})))

(defn day2part1
  []
  (let [computer (init-computer (mapv #(Integer. (str/trim %)) (load-csv 2)) 12 2)]
    (run computer)))

(defn day2part2
  []
  (let [memory (mapv #(Integer. (str/trim %)) (load-csv 2))
        vs (for [i (range 100) j (range 100)] {:noun i :verb j})]
    (reduce
     (fn [_ v]
       (let [r (run (init-computer memory (:noun v) (:verb v)))]
         (if (= 19690720 (get-in r [:pr 0]))
           (reduced v))))
     nil
     vs)))

(comment
  (get-in (day2part1) [:pr 0])
  (let [{:keys [:noun verb]} (day2part2)]
    (+ (* 100 noun) verb))
  )
