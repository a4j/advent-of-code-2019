(ns adventofcode2019.intcode
  (:require [adventofcode2019.core :as core]
            [clojure.core.async :as async]
            [clojure.string :as str]))

(defn iv
  "Get instruction value"
  [offset {:keys [mode pr ip rbo] :as com}]
  (let [p (get pr (+ offset ip))]

    (cond
      (= 0 (get mode (dec offset)))
      (get pr p 0)

      (= 1 (get mode (dec offset)))
      p

      ;; relative mode
      (= 2 (get mode (dec offset)))
      (get pr (+ rbo p) 0))))

(defn pv
  "Get place to put value"
  [offset {:keys [mode pr ip rbo]}]
  (let [p (get pr (+ offset ip))]

    (cond
      (= 0 (get mode (dec offset)))
      p

      (= 1 (get mode (dec offset)))
      (assert false "pv with mode 1")

      (= 2 (get mode (dec offset)))
      (+ rbo p))))

(defn add
  [{:keys [pr ip state mode] :as computer}]
  (let [p1 (iv 1 computer)
        p2 (iv 2 computer)
        p3 (pv 3 computer)]
    (assoc-in computer [:pr p3]
              (+ p1 p2))))

(defn mult
  [{:keys [pr ip state] :as computer}]
  (let [p1 (iv 1 computer)
        p2 (iv 2 computer)
        p3 (pv 3 computer)]
    (assoc-in computer [:pr p3]
              (* p1 p2))))

(defn input
  [{:keys [pr ip state in] :as computer}]
  (let [p1 (pv 1 computer)
        v (async/<!! in)]
    (assoc-in computer [:pr p1] v)))

(defn output
  [{:keys [pr ip state mode out] :as computer}]

  (let [p1 (iv 1 computer)]
    (async/>!! out p1)
    computer))

(defn jump-if-true
  [{:keys [pr ip state mode] :as computer}]

  (let [p1 (iv 1 computer)]
    (if (not (zero? p1))
      (assoc computer :ip (iv 2 computer))
      (update computer :ip #(+ 3 %)))))

(defn jump-if-false
  [{:keys [pr ip state mode] :as computer}]

  (let [p1 (iv 1 computer)]
    (if (zero? p1)
      (assoc computer :ip (iv 2 computer))
      (update computer :ip #(+ 3 %)))))

(defn less-than
  [{:keys [pr ip state mode] :as computer}]

  (let [p1 (iv 1 computer)
        p2 (iv 2 computer)
        p3 (pv 3 computer)]
    (if (< p1 p2)
      (-> computer
          (assoc-in [:pr p3] 1)
          (update :ip #(+ 4 %)))
      (-> computer
          (assoc-in [:pr p3] 0)
          (update :ip #(+ 4 %))))))

(defn equals
  [{:keys [pr ip state mode] :as computer}]

  (let [p1 (iv 1 computer)
        p2 (iv 2 computer)
        p3 (pv 3 computer)]
    (if (= p1 p2)
      (-> computer
          (assoc-in [:pr p3] 1)
          (update :ip #(+ 4 %)))
      (-> computer
          (assoc-in [:pr p3] 0)
          (update :ip #(+ 4 %))))))

(defn adjust-relative-base-offset
  [{:keys [pr ip state mode rbo] :as computer}]

  (let [p1 (iv 1 computer)]
    (-> computer
        (update :rbo #(+ % p1))
        (update :ip #(+ 2 %)))))

(defn opcode
  [i]
  (mod i 100))

(defn modes
  [i]
  (mapv #(Integer. (str %)) (drop 2 (reverse (format "%05d" (int i))))))

(defn tick
  [{:keys [pr ip state] :as computer}]

  (let [instruction (opcode (get pr ip))
        computer (assoc computer :mode (modes (get pr ip)) :instruction instruction)]

    (condp = instruction
      1 (update (add computer) :ip #(+ 4 %))
      2 (update (mult computer) :ip #(+ 4 %))
      3 (update (input computer) :ip #(+ 2 %))
      4 (update (output computer) :ip #(+ 2 %))
      5 (jump-if-true computer)
      6 (jump-if-false computer)
      7 (less-than computer)
      8 (equals computer)
      9 (adjust-relative-base-offset computer)
      99 (assoc computer :state :done)
      (assoc computer :state :error))))

(defn run
  [computer]
  (if (not= :ok (:state computer))
    (do
      (async/close! (:out computer))
      computer)
    (recur (tick computer))))

(comment
  (run {:ip 0
        :state :ok
        :pr (mapv #(Integer. (str/trim %)) (str/split "1,9,10,3,2,3,11,0,99,30,40,50" #","))})

  )

(defn init-computer
  ([memory i o]
   {:pr (zipmap (range) (mapv bigint memory))
    :ip 0
    :state :ok
    :in i
    :out o
    :rbo 0 ;; relative base offset
    })
  ([memory]
   (init-computer memory (async/chan 100) (async/chan 100))))

(comment

  (run
    (init-computer
     [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]))

  (run
    (init-computer
     [1102,34915192,34915192,7,4,7,99,0]))

  (run
    (init-computer
     [104,1125899906842624,99]))
  )
