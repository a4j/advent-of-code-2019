(ns adventofcode2019.day8
  (:require [adventofcode2019.core :as core]
            [clojure.string :as str]))


(defn load-input
  []
  (map #(Integer. %)  (str/split (first (core/load-lines 8)) #"")))

(defn image-layers
  [d w h]
  (partition (* w h) d))

(defn image-f
  [layers]
  (map
   (fn [d]
     {:layer d
      :fs (frequencies d)})
   layers))

(defn part1
  [input w h]

  (let [layer (first
               (sort-by
                #(-> % :fs (get 0))
                (image-f (image-layers input w h))))]
    (* (get-in layer [:fs 1])
       (get-in layer [:fs 2]))))

(defn visible
  [& pixels]
  (first (remove #{2} pixels)))

(defn stack
  [layers]
  (apply
   map
   visible
   (map :layer layers)))

(defn print-image
  [d w h]
  (doseq [l (partition w d)]
    (println (apply str (map {0 \space 1 \x} l)))))



(defn part2
  [input w h]
  (let [layers (image-f (image-layers input w h))]
    (print-image (stack layers) 25 6)))

(comment


  (part1 (load-input) 25 6)
  (part2 (load-input) 25 6)
  )
