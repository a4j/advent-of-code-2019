(ns adventofcode2019.day12
  (:require [adventofcode2019.core :as core]
            [clojure.string :as str]))

(defn parse-moon
  [s]
  (assoc
   (zipmap [:x :y :z] (map #(Integer. %) (rest (re-find #"<x=(-?\d+), y=(-?\d+), z=(-?\d+)" s))))
   :xv 0 :yv 0 :zv 0))

(defn load
  []
  (map parse-moon (core/load-lines 12)))

(defn gravities
  [moon others]
  (reduce
   (fn [m other]
     (-> m
         (update :xv #(cond (> (:x moon) (:x other)) (dec %) (> (:x other) (:x moon)) (inc %) :else %))
         (update :yv #(cond (> (:y moon) (:y other)) (dec %) (> (:y other) (:y moon)) (inc %) :else %))
         (update :zv #(cond (> (:z moon) (:z other)) (dec %) (> (:z other) (:z moon)) (inc %) :else %))))
   moon
   others))

(defn gravities-1
  [moon others v-key p-key]
  (reduce
   (fn [m other]
     (update m v-key
             #(cond
                (> (p-key moon) (p-key other))
                (dec %)

                (> (p-key other) (p-key moon))
                (inc %)

                :else
                %)))
   moon
   others))

(defn velocity
  [{:keys [x y z xv yv zv] :as moon}]
  {:x (+ x xv)
   :y (+ y yv)
   :z (+ z zv)
   :xv xv
   :yv yv
   :zv zv})

(defn velocity-1
  [moon v-key p-key]
  {p-key
   (+ (p-key moon) (v-key moon))

   v-key
   (moon v-key)})

(defn tick
  [moons]
  (for [moon moons]
    (velocity (gravities moon (remove #{moon} moons)))))

(defn tick-1
  [moons v-key p-key]
  (for [moon moons]
    (velocity-1 (gravities-1 moon (remove #{moon} moons) v-key p-key) v-key p-key)))

(defn part1
  [moons n]
  (let [end (nth (iterate tick moons) n)]
    (apply
     +
     (map (fn [{:keys [xv yv zv x y z]}] (* (+ (Math/abs xv) (Math/abs yv) (Math/abs zv))
                                            (+ (Math/abs x) (Math/abs y) (Math/abs z))))
          end))))

(defn find-repeat
  [moons v-key p-key]
  (loop [moons moons
         seen #{}
         i 0]
    (let [step (tick-1 moons v-key p-key)]
      (if (seen step)
        i
        (recur step (conj seen step) (inc i))))))

(defn gcd
  [a b]
  (if (zero? b)
    a
    (recur b, (mod a b))))

(defn lcm
  [a b]
  (/ (* a b) (gcd a b)))
;; to calculate the lcm for a variable number of arguments
(defn lcmv [& v] (reduce lcm v))

(defn part2
  [moons]

  (let [x (find-repeat moons :xv :x)
        y (find-repeat moons :yv :y)
        z (find-repeat moons :zv :z)]
    (lcmv x y z)))

(comment
  (core/load-lines 12)

  (load)
  (parse-moon "<x=6, y=10, z=10>")

  (def sample-s "<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>")

  (def sample (map parse-moon (str/split-lines sample-s)))
  sample

  (velocity
   (gravities
    (first sample) (rest sample)))

  (tick
   (tick
    sample))

  (nth (iterate tick sample) 2)

  (part1 sample 10)
  (part2 (load))


  (def sample-2-s "<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>")

  (def sample-2 (map parse-moon (str/split-lines sample-2-s)))

  (part1 (load) 1000)
  )
