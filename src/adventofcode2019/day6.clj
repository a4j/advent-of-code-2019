(ns adventofcode2019.day6
  (:require [adventofcode2019.core :as core]
            [clojure.set :as set]
            [clojure.string :as str]))

(defn count-orbits
  [m parent depth]
  (+ (* depth (count (get m parent)))
     (apply + (map #(count-orbits m % (inc depth)) (get m parent)))))

(defn part1
  [ls]
  (let [ls (map #(str/split % #"\)") ls)
        orbits
        (reduce
         (fn [m [k v]] (update m k conj v))
         {}
         ls)]
    (count-orbits orbits "COM" 1)))

(defn path-to
  [m d]
  (if (= d "COM")
    []
    (conj (path-to m (m d)) (m d))))

(defn part2
  [ls]
  (let [ls (map #(str/split % #"\)") ls)
        reverse-orbits
        (reduce
         (fn [m [k v]] (assoc m v k))
         {}
         ls)
        you-path (path-to reverse-orbits "YOU")
        san-path (path-to reverse-orbits "SAN")]
    (+
     (count (set/difference (set you-path) (set san-path)))
     (count (set/difference (set san-path) (set you-path))))))

(comment
  (part1 g)
  (part2 (core/load-lines 6))
  (part1 (core/load-lines 6))





  (def s "COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN")
  (def g (str/split-lines s)))
