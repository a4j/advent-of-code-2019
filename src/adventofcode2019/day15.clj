(ns adventofcode2019.day15
  (:require [adventofcode2019.core :as core]
            [adventofcode2019.intcode :as intcode]
            [clojure.core.async :as async]
            [clojure.set :as set]
            [clojure.string :as str]))

(defn load
  []
  (map str/trim (core/load-csv 15)))

(defn move
  [c d]
  (async/>!! (:in c) d)
  [c (async/<!! (:out c))])

(defn adjacent
  [[x y]]
  #{[(inc x) y] [(dec x) y] [x (inc y)] [x (dec y)]})

(defn next-unknown
  "Returns the unknown directly from point, or nil if there isn't one"
  [unknowns p]
  (first (set/intersection (adjacent p) unknowns)))

(defn move-direction
  "The direction of the move from x1, y1 to x2, y2"
  [[x1 y1] [x2 y2]]
  (cond
    (> x1 x2) 3
    (> x2 x1) 4
    (> y1 y2) 1
    (> y2 y1) 2
    :else (throw (ex-info "Invalid move direction" {:source [x1 y1] :dest [x2 y2]}))))

;; 1 north
;; 2 south
;; 3 west
;; 4 east
(defn reverse-direction
  [d]
  ({1 2 2 1 3 4 4 3} d))

(defn unknowns-from-point
  "insert into unknowns everything that touches [x y] that isn't in m"
  [m unknowns p]
  (set/union
   unknowns
   (set/difference
    (adjacent p)
    (set (keys m)))))

(def point-description
  {0 \#
   1 \.
   2 \b})

(defn explore
  ([c m unknowns path]
   (if (empty? unknowns)
     m

     (if-let [u (next-unknown unknowns (first path))]
       (let [dir   (move-direction (first path) u)
             [c r] (move c dir)]

         (if (zero? r)
           (recur c (assoc m u \#) (disj unknowns u) path)
           (recur
            c
            (assoc m u (point-description r))
            (disj (unknowns-from-point m unknowns u) u)
            (conj path (conj u dir)))))
       (recur (first (move c (reverse-direction (nth (first path) 2)))) m unknowns (rest path)))))

  ([c]
   (let [f (future (intcode/run c))]

     (try
       (explore c {[0 0] \.} #{[-1 0] [1 0] [0 -1] [0 1]} '([0 0 nil]))

       (finally (future-cancel f))))))

(defn achievable
  [m current]
  (let [a (adjacent current)]
    (set (filter #(not= \# (m % \#)) a))))

(defn not-seen
  [points unvisited]
  (set/intersection points (set (keys unvisited))))

(defn smallest-unvisited
  [unvisited]
  (first (first (sort-by second unvisited))))

(defn update-scores
  [m current unvisited visited]
  (let [neighbours (not-seen (achievable m current) unvisited)]
    (reduce
     (fn [un neighbour]
       (let [current-score (unvisited current)]
         (if (< (inc current-score) (un neighbour))
           (assoc un neighbour (inc current-score))
           un)))
     (dissoc unvisited current)
     neighbours)))

(defn dijkstra
  ([m current unvisited visited destination]

   (if-let [result (visited destination)]

     result

     (let [new-unvisited (update-scores m current unvisited visited)]
       (recur m
              (smallest-unvisited new-unvisited)
              new-unvisited
              (assoc visited current (unvisited current))
              destination))))

  ([m destination]
   (dijkstra m [0 0] (assoc (zipmap (keys m) (repeat Integer/MAX_VALUE)) [0 0] 0) {} destination)))

(defn targetless-dijkstra
  ([m current unvisited visited]

   (if (empty? unvisited)

     visited

     (let [new-unvisited (update-scores m current unvisited visited)]
       (recur m
              (smallest-unvisited new-unvisited)
              new-unvisited
              (assoc visited current (unvisited current))))))

  ([m start]
   (targetless-dijkstra m start (assoc (zipmap (keys m) (repeat Integer/MAX_VALUE)) start 0) {})))

(defn part1
  ([]
   (let [whole (explore (intcode/init-computer (load)))
         oxygen (first (first (filter #(= \b (val %)) whole)))]

     (dijkstra whole oxygen)))
  ([whole oxygen]
   (dijkstra whole oxygen)))

(defn part2
  [whole oxygen]
  (first (reverse
          (sort-by val
                   (remove
                    #(= Integer/MAX_VALUE (val %))
                    (targetless-dijkstra whole oxygen))))))

(comment

  (part1)

  (conj [1 2] :x)

  (disj #{1 2 3} 3)


  (def whole (explore (intcode/init-computer (load))))
  (def oxygen (first (first (filter #(= \b (val %)) whole))))
  (try
    (part1 whole oxygen)
    (catch Exception _ :error))

  (part2 whole oxygen)


  )
