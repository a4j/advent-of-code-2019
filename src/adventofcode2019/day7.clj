(ns adventofcode2019.day7
  (:require [adventofcode2019.core :as core]
            [adventofcode2019.intcode :as ic]
            [clojure.core.async :as async]
            [clojure.math.combinatorics :as combo]
            [clojure.string :as str]))

;; p (list phase-setting input-v)

(defn run-new
  [p i-chan o-chan]
  (ic/run (ic/init-computer p i-chan o-chan)))

(defn amp-circuit
  [p [pa pb pc pd pe]]
  (let [world->a (async/chan 3)
        a->b (async/chan 3)
        b->c (async/chan 3)
        c->d (async/chan 3)
        d->e (async/chan 3)
        e->world (async/chan 3)]

    (async/>!! world->a pa)
    (async/>!! world->a 0)

    (async/>!! a->b pb)
    (async/>!! b->c pc)
    (async/>!! c->d pd)
    (async/>!! d->e pe)

    (let [a (run-new p world->a a->b)
          b (run-new p a->b b->c)
          c (run-new p b->c c->d)
          d (run-new p c->d d->e)
          e (run-new p d->e e->world)]

      (async/<!! e->world))))

(defn feedback-amp-circuit
  [p [pa pb pc pd pe]]
  (let [e->a (async/chan 3)
        a->b (async/chan 3)
        b->c (async/chan 3)
        c->d (async/chan 3)
        d->e (async/chan 3)]

    (async/>!! e->a pa)
    (async/>!! e->a 0)

    (async/>!! a->b pb)
    (async/>!! b->c pc)
    (async/>!! c->d pd)
    (async/>!! d->e pe)


    (let [a (future (run-new p e->a a->b))
          b (future (run-new p a->b b->c))
          c (future (run-new p b->c c->d))
          d (future (run-new p c->d d->e))
          e (run-new p d->e e->a)]

      (async/<!! e->a))))

(defn part1
  [p]
  (let [phase-perms (combo/permutations [0 1 2 3 4])]
    (first (reverse (sort (map (partial amp-circuit p) phase-perms))))))

(defn part2
  [p]
  (let [phase-perms (combo/permutations [9 8 7 6 5])]
    (first (reverse (sort (map (partial feedback-amp-circuit p) phase-perms))))))

(def prog (mapv #(Integer. %) (str/split (first (core/load-lines 7)) #",")))

(comment



  (def egprog [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5])
  (def egprog2 [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10])

  (amp-circuit prog [0 1 2 3 4])
  (feedback-amp-circuit egprog2 [9 7 8 5 6])

  (part2 prog)
  )
