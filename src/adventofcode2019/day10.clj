(ns adventofcode2019.day10
  (:require [adventofcode2019.core :as core]
            [clojure.set :as set]
            [clojure.string :as str]))

(defn load-input
  []
  (core/load-lines 10))

(defn parse
  "The coordinates of the asteroids"
  [lines]

  (into []
        (for [i (range (count lines))
              j (range (count (first lines)))
              :let [v (get-in lines [i j])]
              :when (= v \#)]
          [j i])))


(defn v-
  [[x1 y1] [x2 y2]]
  [(- x1 x2) (- y1 y2)])

(defn v+
  [[x1 y1] [x2 y2]]
  [(+ x1 x2) (+ y1 y2)])

(comment
  (v- [3 2] [1 1])
  )

(defn line?
  [[ax ay] [bx by]]
  (= (/ ax ay) (/ bx by)))

(defn distance
  [[x1 y1] [x2 y2]]
  (Math/sqrt (+ (Math/pow (- x2 x1) 2)
                (Math/pow (- y2 y1) 2))))

(defn dot-product
  [[x1 y1] [x2 y2]]
  (+ (* x1 x2) (* y1 y2)))

(defn magnitude
  [[x y]]
  (Math/sqrt (+ (* x x) (* y y))))

(defn codirectional?
  [a b]

  (let [dp (dot-product a b)
        mp (* (magnitude a) (magnitude b))
        cos-theta (/ dp mp)]
    (<= 0.9999999 cos-theta 1.000001)))


(comment

  (codirectional?
   [2 3]
   [4 6])
  )

(defn closer?
  "Is a closer to the origin than b?"
  [a b]

  (< (distance [0 0] a) (distance [0 0] b)))

(comment
  (= true (line? [1 1] [2 2] [3 3]))

  (= true (closer? [1 1] [3 3] [4 4]))
  (= false (closer? [1 1] [3 3] [2 2]))
  )

(defn obscures?
  [o1' o2']
  (and
   (codirectional? o1' o2')
   (closer? o1' o2')))


(defn count-visible

  [field asteroid]

  (apply +
         (for [object1 (set/difference field #{asteroid})
               :let [o1' (v- object1 asteroid)]]
           (if (some #(obscures? (v- % asteroid) o1') (set/difference field #{asteroid object1}))
             0
             1))))

(defn counts
  [asteroids]
  (into {}
        (for [asteroid asteroids]
          [asteroid (count-visible asteroids asteroid)])))

(defn atan2
  [[x y]]
  (Math/atan2 x y))

(defn angle
  [[x1 y1] [x2 y2]]
  (Math/atan2 (- x1 x2) (- y1 y2)))

(defn part1
  [asteroids]
  (->> (counts asteroids)
       (sort-by second)
       reverse
       ffirst))
;; [11 19]
;; 253

(defn part2
  [p asteroids]

  (->> (group-by #(angle % p) asteroids)
       (sort-by first)
       reverse
       (map (fn [[k v]] (lazy-cat (sort-by #(distance p %) v) (repeat nil))
              ))
       (apply interleave)
       (remove nil?)
       (drop 199)
       first
       )
  )

(comment

  (count (mapcat second (part2 [11 19] (parse (load-input)))))

  (def others (remove #{[11 19]} (parse (load-input))))
  (part2 [11 19] others)[8 15]
  (part2 [11 13] (remove #{[11 13]} eg))
  (=  (angle [11 12] [11 13])
      (angle [11 11] [11 13]))

  (atan2 [0 1])

  (->> [[0 1] [-1 1] [-1 0] [-1 -1] [0 -1] [1 -1] [1 0] [1 1]]
       (group-by #(atan2 %))
       (sort-by first)
       reverse
       (mapcat second)
       )

  (def egs ".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##")
  (def eg (parse (clojure.string/split-lines egs)))

  (some #{[11 13]} (remove #{[11 13]} eg))



  )
