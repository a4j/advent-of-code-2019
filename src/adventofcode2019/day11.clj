(ns adventofcode2019.day11
  (:require [adventofcode2019.core :as core]
            [adventofcode2019.intcode :as intcode]
            [clojure.core.async :as async]
            [clojure.string :as str]))

(defn log [s]
  (spit "/tmp/out" s :append true)
  (spit "/tmp/out" "\n" :append true))

(defn init
  ([]
   {:direction 0
    :surface {}
    :position [0 0]})
  ([init-surface]
   {:direction 0
    :surface init-surface
    :position [0 0]}))

(defn turn
  [direction movement]
  (if (= 0 movement)
    (mod (dec direction) 4)
    (mod (inc direction) 4)))

(defn v+
  [[x1 y1] [x2 y2]]
  [(+ x1 x2) (+ y1 y2)])

(def moves
  {0 [0 -1]
   3 [-1 0]
   2 [0 1]
   1 [1 0]})

(defn move
  [painting]
  (update painting :position
          (fn [p]
            (v+ p (moves (:direction painting))))))

(comment
  (turn 3 1)
  )

(defn report
  [painting out]
  (async/>!! out (get-in painting [:surface (:position painting)] 0))
  painting)

(defn paint
  [painting colour movement out]
  (-> painting
      (update :surface assoc (:position painting) colour)
      (update :direction turn movement)
      move
      (report out)))

(defn painter
  [painting in out]
  (report painting out)
  (loop [colour (async/<!! in)
         painting painting]
    (if colour
      (let [movement (async/<!! in)
            np (paint painting colour movement out)]
        (log (assoc (dissoc np :surface) :movement movement))
        (recur (async/<!! in) np))
      painting)))

(defn reset-log
  []
  (spit "/tmp/out" "\n"))

(defn part1

  []
  (reset-log)
  (let [p-in (async/chan 10)
        p-out (async/chan 10)
        c (intcode/init-computer (map str/trim (core/load-csv 11)) p-out p-in)
        c-fn (future (intcode/run c))]
    (painter (init) p-in p-out)

    ))

(defn part2

  []
  (reset-log)
  (let [p-in (async/chan 10)
        p-out (async/chan 10)
        c (intcode/init-computer (map str/trim (core/load-csv 11)) p-out p-in)
        c-fn (future (intcode/run c))]
    (painter (init {[0 0] 1}) p-in p-out)

    ))

(defn extent
  [s]
  (let [k (keys s)]
    {:x-min
     (first (sort (map first k)))
     :x-max
     (first (reverse (sort (map first k))))
     :y-min
     (first (sort (map second k)))
     :y-max
     (first (reverse (sort (map second k))))}))

(extent {[20 30] :x [-1 1] :y});; [-1 20 1 30]

(defn show-surface
  [s]
  (let [{:keys [x-min x-max y-min y-max]} (extent s)]
    (doseq [i (range x-min (inc x-max))]
      (doseq [j (range y-min (inc y-max))]
        (print (if (= 1 (s [i j])) \x \space)))
      (print \newline))))

(comment


  (intcode/init-computer (map str/trim (core/load-csv 11)))
  (def r  (part1))
  (def r2 (part2))

  (show-surface (:surface r2))

  (count (:surface r))

  )
