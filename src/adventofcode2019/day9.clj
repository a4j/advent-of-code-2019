(ns adventofcode2019.day9
  (:require [adventofcode2019.core :as core]
            [adventofcode2019.intcode :as intcode]
            [clojure.core.async :as async]
            [clojure.string :as str]))

(defn load
  []
  (map str/trim (core/load-csv 9)))

(defn part1
  [p]
  (let [i (async/chan 3) o (async/chan 3)]
    (async/>!! i 1)
    (async/<!! (:out (intcode/run (intcode/init-computer p i o))))))

(defn part2
  [p]
  (let [i (async/chan 3) o (async/chan 3)]
    (async/>!! i 2)
    (async/<!! (:out (intcode/run (intcode/init-computer p i o))))))

(comment

  (part1 (load))
  (part2 (load))
  )
