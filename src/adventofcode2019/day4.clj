(ns adventofcode2019.day4)

(defn p1-valid?
  [n]
  (let [digits (map #(Integer. (str %)) (str n))]
    (and (= (count digits) 6)
         (<= 402328 n 864247)
         (some (fn [[a b]] (= a b)) (partition 2 1 digits))
         (= digits (sort digits)))))

(defn p2-valid?
  [n]
  (let [digits (map #(Integer. (str %)) (str n))]
    (and (= (count digits) 6)
         (<= 402328 n 864247)
         (some (fn [[a b]] (= a b)) (partition 2 1 digits))
         (= digits (sort digits))
         (some #{2} (map count (partition-by identity digits))))))

(defn part1
  []
  (count (filter p1-valid? (range 402328 (inc 864247)))))

(defn part2
  []
  (count (filter p2-valid? (range 402328 (inc 864247)))))

(comment
  (valid? 402329)

  (part1)
  (part2)
  )

(some #{2} (map count (partition-by identity [1 1 1 1 2 2 2])))
