(ns adventofcode2019.day19
  (:require [adventofcode2019.core :as core]
            [adventofcode2019.intcode :as intcode]
            [clojure.core.async :as async]
            [clojure.string :as str]))

(defn load-day
  []
  (map str/trim (core/load-csv 19)))

(defn make-computer
  [program i o]

  (intcode/init-computer program i o))

(defn present?
  [program [x y]]
  (let [i (async/chan 3)
        o (async/chan 3)
        c (make-computer program i o)]

    (future (intcode/run c))

    (async/>!! i x)
    (async/>!! i y)
    (not (zero? (async/<!! o)))))

(defn part1
  [program]
  (reduce

   (fn [a p] (if (present? program p) (inc a) a))
   0
   (for [i (range 50) j (range 50)] [i j])))

(defn fits?
  [program [x y]]
  (present? program [(+ x 99) (- y 99)]))

(defn part2
  [program]
  (loop [x 0 y 100]
    (if (present? program [x y])
      (if (fits? program [x y])
        [x y]
        (recur x (inc y)))
      (recur (inc x) y))))

(defn show
  [program w h]
  (doseq [y (range h)]
    (doseq [x (range w)]
      (if (present? program [x y])
        (print \#)
        (print \.)))
    (print \newline)))


(comment


  (part1 (load-day))
  (def a (part2 (load-day)))
  (+ (* 10000 (first a)) (- (second a) 99))


  (show (load-day) 10 10)
  (present? (load-day ) [9 8])
  )
