(ns adventofcode2019.day14
  (:require [adventofcode2019.core :as core]
            [clojure.string :as str]))

(defn parse-element
  [s]
  (let [[_ n e] (re-find #"(\d+) (\w+)" s)]
    [(Integer. n) e]))

(comment
  (parse-element "171 ORE")

  )

(defn parse-line
  [s]
  (let [[_ pre post] (re-find #"^(.*) => (.*)$" s)]
    [(mapv parse-element (str/split pre #", ")) (parse-element post)]))

(comment
  (parse-line "7 A, 1 C => 1 D")
  (parse-line "7 A => 1 D")
  )

(defn parse
  [lines]
  (mapv parse-line lines))


(comment
  (parse-line "7 A, 1 B => 1 C")

  ;;[[[7 "A"] [1 "B"]] [1 "C"]]
  )
(defn create-recipe
  [instructions]
  (reduce
   (fn [m [pre [n target]]]
     (assoc m target {:n n :pre pre}))
   {}
   instructions))


(comment
  (parse-line "7 A, 1 C => 1 D")
  ;;[[[7 "A"] [1 "C"]] [1 "D"]]

  (produce-need
   r
   "A"
   9)  ;; [{"ORE" 10} {"A" 1}]

  )

(defn first-want
  "the first want that isn't ORE"
  [want]
  (->> want
       (remove #(= "ORE" (key %)))
       (remove #(= 0 (val %)))
       first))

(comment
  (first-want {"ORE" 3}) ;;nil
  (first-want {"ORE" 3 "A" 0 "XYZ" 10});; ["XYZ" 10]
  )

(defn use-store
  [have e n-e]
  (if-let [n (have e)]
    (assoc have e (max 0 (- n n-e)))
    have))

(defn satisfy
  [recipe have want e]

  (if (>= (have e 0) (want e 0))

    [(update have e (fnil #(- % (want e 0)) 0))
     (dissoc want e)]


    (let [remain          (- (want e) (have e 0))
          have            (dissoc have e)
          want            (dissoc want e)
          {:keys [n pre]} (recipe e)
          ntimes          (bigint (Math/ceil (/ remain n)))]

      [(assoc have e (- (* ntimes n) remain))
       (reduce
        (fn [m [w-n w-e]]
          (update m w-e (fnil #(+ % (* ntimes w-n)) 0)))
        want
        pre)])))

(comment
  (use-store {"X" 3} "X" 2)

  (satisfy r {"A" 7} {"A" 18} "A")
  )

(defn solve
  [recipe have want]

  (if-let [[e _] (first-want want)]
    (let [[have want] (satisfy recipe have want e)]
      (recur recipe have want))
    (want "ORE"))
  )

(defn parse-sample
  [s]
  (-> s
      str/split-lines
      parse
      create-recipe)
  )

(defn part1
  []
  (let [lines (core/load-lines 14)]
    (-> lines
        parse
        create-recipe
        (solve {} {"FUEL" 1}))))

(defn binary-search-gte
  [items value f]
  (letfn
      [(do-search
         [low high]
         (if (< low high)
           (let [mid (quot (+ low high) 2)]
             (if (> value (f (items mid)))
               (recur (inc mid) high)
               (recur low mid)))
           low))]
    (do-search 0 (count items))))

(defn part2
  [r]
  (let [f #(solve r {} {"FUEL" %})]
    (binary-search-gte (vec (range 0 4100000)) 1000000000000 f)))

(comment

  (part1)

  (let [lines (core/load-lines 14)
        recipe (-> lines parse create-recipe)]

    (part2 recipe))


  (def sample-1-s "10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL")

  (def sample-2-s "9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL")

  (def sample-3-s "171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX")

  (def r (parse-sample sample-1-s))
  (def r2 (parse-sample sample-2-s))
  (def r3 (parse-sample sample-3-s))
  (solve r3 {} {"FUEL" 1})

  )
