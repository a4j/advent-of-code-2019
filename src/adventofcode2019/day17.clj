(ns adventofcode2019.day17
  (:require [adventofcode2019.core :as core]
            [adventofcode2019.intcode :as intcode]
            [clojure.core.async :as async]
            [clojure.string :as str]))

(defn load-day
  []

  (map #(Integer. %) (map str/trim (core/load-csv 17))))


(defn read-all
  [chan]
  (async/go-loop [c (async/<! chan)
                  r []]
    (if c
      (recur (async/<! chan) (conj r c))
      r)))

(defn get-map
  [input]
  (let [in (async/chan 3)
        out (async/chan 3)
        r (read-all out)]

    (intcode/run (intcode/init-computer input in out))
    r))

(defn parse-map
  [s]
  (first
   (reduce
    (fn [[m x y] c]
      (if (= \newline (char c))
        [m 0 (inc y)]

        [(assoc m [x y] (char c)) (inc x) y]))
    [{} 0 0]
    s)))

(defn surrounding
  [[x y]]
  [[(inc x) y] [(dec x) y] [x (inc y)] [x (dec y)] [x y]])

(defn intersections
  "Any point where the point and four surrounding points are hashes"
  [m]

  (filter

   (fn [[p _]]
     (every? #{\#} (map (fn [s] (get m s)) (surrounding p))))

   m))

(defn part1
  [m]
  (let [ixns (intersections m)]

    (reduce
     (fn [sum [[x y] _]]
       (+ sum (* x y)))
     0
     ixns)))

(def program
  ["A,B,B,C,C,A,B,B,C,A\n"
   "R,4,R,12,R,10,L,12\n"
   "L,12,R,4,R,12\n"
   "L,12,L,8,R,10\n"
   "n\n"])

(defn to-ic
  [p]
  (mapcat
   (fn [line]
     (mapcat
      (fn [x] (map int x))
      (interpose "," (str/split line #","))))
   p))

(defn part2
  [p ixns]
  (let [in (async/chan 100)
        out (async/chan 100)
        c (intcode/init-computer (conj (rest p) 2) in out)]

    (async/go-loop [ixns ixns]
      (when (first ixns)
        (async/>! in (first ixns))
        (recur (rest ixns))))

    (let [result
          (read-all out)]

      (future (intcode/run c))
      (async/<!! result))))

(comment

  (load-day)
  (char 66)

  (def a (async/<!! (get-map (load-day))))
  (part1 (parse-map a))

  (part2 (load-day)
         (to-ic program))
  )
