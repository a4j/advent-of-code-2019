(ns adventofcode2019.day3
  (:require [adventofcode2019.core :as core]
            [clojure.set :as set]
            [clojure.string :as str]))

(def ords
  {\U [0 1]
   \D [0 -1]
   \L [-1 0]
   \R [1 0]})

(defn s->vecs
  "Turns string U3 into three up vectors"
  [s]
  (let [c (Integer. (apply str (drop 1 s)))
        d (ords (first s))]
    (repeat c d)))

(defn v+
  "Add two vectors"
  [[x1 y1] [x2 y2]]
  [(+ x1 x2)
   (+ y1 y2)])

(defn md
  "Manhattan distance of vector from origin"
  [[x y]]
  (+ (Math/abs x) (Math/abs y)))

(comment
  (v+ [2 3] [4 5])

  (s->vecs "D3"))

(defn string->line
  [s]
  (reductions v+ [0 0] (mapcat s->vecs (str/split s #","))))

(defn intersections
  [line1 line2]
  (let [line1positions (into #{} line1)
        line2positions (into #{} line2)

        intersections (set/intersection line1positions line2positions)]

    (set/difference intersections #{[0 0]})))

(defn day3part1
  ([line1 line2]
   (first (sort (map md (intersections line1 line2)))))

  ([]
   (let [input (core/load-lines 3)
         line1 (string->line (first input))
         line2 (string->line (second input))]
     (day3part1 line1 line2)
     (first (sort (map md (intersections line1 line2)))))))

(comment
  (day3part1)
  )

(defn costs
  [path]
  (:costs
   (reduce
    (fn [{:keys [costs length] :as m}
         p]
      (if-let [steps (costs p)]
        (assoc m :length (inc length))
        (-> m
            (assoc-in [:costs p] length)
            (assoc :length (inc length)))))
    {:costs {[0 0] 0}
     :length 0}
    path)))

(comment
  (costs [[0 0] [0 1] [0 2] [0 1] [1 1] [2 1] [1 1] [1 0]])
  )

(defn day3part2
  ([line1 line2]
   (let [line1costs (costs line1)
         line2costs (costs line2)

         distances
         (map
          (fn [i]
            [i
             (+ (line1costs i) (line2costs i))])
          (intersections line1 line2))]
     (sort-by second distances)))

  ([]
   (let [input (core/load-lines 3)
         line1 (string->line (first input))
         line2 (string->line (second input))]
     (day3part2 line1 line2))))

(comment
  (first (day3part2))

  (first
   (day3part2
    (string->line "R75,D30,R83,U83,L12,D49,R71,U7,L72")
    (string->line "U62,R66,U55,R34,D71,R55,D58,R83")))

  (first
   (day3part2
    (string->line "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51")
    (string->line "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")))

  (intersections
   (string->line "R8,U5,L5,D3")
   (string->line "U7,R6,D4,L4"))
  )
