(ns adventofcode2019.day16
  (:require [adventofcode2019.core :as core]))

(defn from-string
  [s]
  (map #(Integer. (str %)) s))

(defn load-day
  []
  (from-string (first (core/load-lines 16))))

(defn phase-position
  [n xs]
  (let [pattern (cycle (concat (repeat (inc n) 0) (repeat (inc n) 1) (repeat (inc n) 0) (repeat (inc n) -1)))
        pattern (drop 1 pattern)]
    (Math/abs (rem (apply + (map * xs pattern)) 10))))

(defn phase
  [xs]
  (mapv (fn [i _] (phase-position i xs)) (range) xs))

(defn part1
  [xs]
  (apply str (take 8 (nth (iterate phase xs) 100))))

(defn phase-2-back
  [xs]
  (reductions
   (fn [a b] (mod (+ a b) 10))
   xs))

(defn part2
  [xs]
  (let [offset (Integer. (apply str (take 7 xs)))
        whole (apply concat (repeat 10000 xs))
        n (apply str (take 8 (reverse (nth (iterate phase-2-back (reverse (drop offset whole))) 100))))]
    n))

(comment
  (load-day)

  (part1 (from-string "80871224585914546619083218645595"))
  (part1 (load-day))

  (part2 (from-string "02935109699940807407585447034323"))
  (part2 (load-day))
  )
